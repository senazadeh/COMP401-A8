package sushigame.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import comp401sushi.Plate;
import sushigame.model.Belt;
import sushigame.model.BeltEvent;
import sushigame.model.BeltObserver;

public class BeltView extends JPanel implements BeltObserver, ActionListener {

	private Belt belt;
	private JButton[] belt_labels;

	public BeltView(Belt b) {
		this.belt = b;
		belt.registerBeltObserver(this);
		setLayout(new GridLayout(belt.getSize(), 1));
		belt_labels = new JButton[belt.getSize()];
		for (int i = 0; i < belt.getSize(); i++) {
			JButton plabel = new JButton("");
			plabel.setMinimumSize(new Dimension(300, 20));
			plabel.setPreferredSize(new Dimension(300, 20));
			plabel.setOpaque(true);
			// plabel.setContentAreaFilled(true);
			plabel.setBackground(Color.GRAY);
			add(plabel);
			belt_labels[i] = plabel;
			plabel.addActionListener(this);
			plabel.setActionCommand(i + "");
		}
		refresh();
	}

	@Override
	public void handleBeltEvent(BeltEvent e) {	
		refresh();
	}

	private void refresh() {
		for (int i = 0; i < belt.getSize(); i++) {
			Plate p = belt.getPlateAtPosition(i);
			JButton plabel = belt_labels[i];
			if (p == null) {
				plabel.setText("");
				plabel.setBackground(Color.GRAY);
			} else {
				plabel.setText(p.toString() + " (Info)");
				switch (p.getColor()) {
					case RED:
						plabel.setBackground(Color.RED); break;
					case GREEN:
						plabel.setBackground(Color.GREEN); break;
					case BLUE:
						plabel.setBackground(Color.BLUE); break;
					case GOLD:
						plabel.setBackground(Color.YELLOW); break;
				}
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int x = Integer.parseInt(e.getActionCommand());
		Plate p = belt.getPlateAtPosition(x);
		if (p == null) {
			JOptionPane.showMessageDialog(this, "No Plate");
		} else if (!(p.getContents().getShortName() == "Roll")) {
			JOptionPane.showMessageDialog(this, "Plate Color: " + p.getColor() + "\n" 
											+ "Sushi Type: " + p.getContents().getName() +  "\n" 
											+ "Chef: " + p.getChef().getName() +  "\n"
											+ "Plate Age: " + belt.getAgeOfPlateAtPosition(x));
		} else {
			String ingredients = "";
			for (int i = 0; i < p.getContents().getIngredients().length; i++) {
				ingredients += p.getContents().getIngredients()[i].getName() + "";
				if (i+1 < p.getContents().getIngredients().length) {
					ingredients += ", ";
				}
			}
			JOptionPane.showMessageDialog(this, "Plate Color: " + p.getColor() + "\n" 
											+ "Sushi Type: " + p.getContents().getName() + "\n" 
											+ "Ingredients: " + ingredients + "\n"
											+ "Chef: " + p.getChef().getName() + "\n"
											+ "Plate Age: " + belt.getAgeOfPlateAtPosition(x));
		}
	}
}
