package sushigame.view;

import java.util.Comparator;

import sushigame.model.Chef;

public class HighToLowSpoiledComparator implements Comparator<Chef> {

	@Override
	public int compare(Chef a, Chef b) {
		return (int) (Math.round(b.foodSpoiledByWeight()*100.0) - 
				Math.round(a.foodSpoiledByWeight()*100));
	}

}
