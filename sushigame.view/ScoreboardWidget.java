package sushigame.view;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import sushigame.model.Belt;
import sushigame.model.BeltEvent;
import sushigame.model.BeltObserver;
import sushigame.model.Chef;
import sushigame.model.SushiGameModel;

public class ScoreboardWidget extends JPanel implements BeltObserver, ActionListener {

	private SushiGameModel game_model;
	private JLabel display;
	private JButton a;
	private JButton b;
	private JButton c;
	private int sortnumber = 0;

	public ScoreboardWidget(SushiGameModel gm) {
		game_model = gm;
		game_model.getBelt().registerBeltObserver(this);

		display = new JLabel();
		display.setVerticalAlignment(SwingConstants.TOP);
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(display);
		display.setText(makeScoreboardHTML(sortnumber));
		/*
		String[] catagories = new String[] {"Balance", "Food Consumed By Weight", "Food Spoiled By Weight"};
		JComboBox<String> sorted = new JComboBox<String>(catagories);
		sorted.setSelectedIndex(0);
		sorted.addActionListener(this);
		this.add(sorted, BorderLayout.SOUTH);
		this.sorted = sorted;
		 */
		JButton a = new JButton("Sort By Balnce");
		JButton b = new JButton("Sort By Weight Sold");
		JButton c = new JButton("Sort By Weight Spoiled");
		this.a = a;
		this.b = b;
		this.c = c;
		add(a);
		add(b);
		add(c);
		a.addActionListener(this);
		a.setActionCommand("a");
		b.addActionListener(this);
		b.setActionCommand("b");
		c.addActionListener(this);	
		c.setActionCommand("c");
	}

	private String makeScoreboardHTML(int sort) {
		String sb_html = "<html>";
		sb_html += "<h1>Scoreboard</h1>";

		Chef[] opponent_chefs= game_model.getOpponentChefs();
		Chef[] chefs = new Chef[opponent_chefs.length+1];
		chefs[0] = game_model.getPlayerChef();
		for (int i=1; i<chefs.length; i++) {
			chefs[i] = opponent_chefs[i-1];
		}	

		switch (sort) {
		case 0:
			Arrays.sort(chefs, new HighToLowBalanceComparator());
			for (Chef c : chefs) {
				sb_html += c.getName() + " ($" + Math.round(c.getBalance()*100.0)/100.0 + ") <br>";
			}
			break;
		case 1:
			Arrays.sort(chefs, new HighToLowSoldComparator());
			for (Chef c : chefs) {
				sb_html += c.getName() + " (" + Math.round(c.foodSoldByWeight()*100.0)/100.0 + " oz) <br>";
			}
			break;
		case 2:
			Arrays.sort(chefs, new HighToLowSpoiledComparator());
			for (Chef c : chefs) {
				sb_html += c.getName() + " (" + Math.round(c.foodSpoiledByWeight()*100.0)/100.0 + " oz) <br>";
			}
			break;
		}
		sb_html += "<h2>Sort By</h2>";
		return sb_html;
	}

	public void refresh() {
		display.setText(makeScoreboardHTML(sortnumber));		
	}

	@Override
	public void handleBeltEvent(BeltEvent e) {
		if (e.getType() == BeltEvent.EventType.ROTATE) {
			refresh();
		}		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		case "a":
			this.sortnumber = 0;
			refresh();
			break;
		case "b":
			this.sortnumber = 1;
			refresh();
			break;
		case  "c":
			this.sortnumber = 2;
			refresh();
			break;
		}
	}
}
