package sushigame.view;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;

import comp401sushi.AvocadoPortion;
import comp401sushi.EelPortion;
import comp401sushi.IngredientPortion;
import comp401sushi.Nigiri;
import comp401sushi.Plate;
import comp401sushi.RedPlate;
import comp401sushi.Roll;
import comp401sushi.Sashimi;
import comp401sushi.SeaweedPortion;
import comp401sushi.Sushi;

public class PlayerChefView extends JPanel implements ActionListener {

	private List<ChefViewListener> listeners;
	private Sushi kmp_roll;
	private Sushi crab_sashimi;
	private Sushi eel_nigiri;
	private int belt_size;
	
	public PlayerChefView(int belt_size) {
		this.belt_size = belt_size;
		listeners = new ArrayList<ChefViewListener>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

		JLabel empty = new JLabel(" ");
				
		String[] sushiTypes = new String[] {"Nigri", "Sashimi", "Roll"};
		JComboBox<String> sushiType = new JComboBox<String>(sushiTypes);
		sushiType.addActionListener(this);
		JLabel selectSushiType =new JLabel("Select Sushi Type");
		add(empty);
		add(selectSushiType);
		add(sushiType);
		
		String[] plateColors = new String[] {"Red Plate", "Blue Plate", "Green Plate", "Gold Plate"};
		JComboBox<String> plateColor = new JComboBox<String>(plateColors);
		plateColor.addActionListener(this);
		JLabel selectPlateColor = new JLabel("Select Plate Color");
		add(selectPlateColor);
		add(plateColor);
		
		JSlider goldPlatePrice = new JSlider(50, 100);
		goldPlatePrice.setPaintTrack(true); 
		goldPlatePrice.setPaintTicks(true); 
		goldPlatePrice.setPaintLabels(true); 
		goldPlatePrice.setMajorTickSpacing(5);
		Hashtable<Integer,JLabel> labelTable = new Hashtable<Integer,JLabel>();
		labelTable.put(new Integer(50), new JLabel("$5"));
		labelTable.put(new Integer(60), new JLabel("$6"));
		labelTable.put(new Integer(70), new JLabel("$7"));
		labelTable.put(new Integer(80), new JLabel("$8"));
		labelTable.put(new Integer(90), new JLabel("$9"));
		labelTable.put(new Integer(100), new JLabel("$10"));
		JLabel goldPlate = new JLabel("Select Price (Only for Gold Plate)");
		goldPlatePrice.setLabelTable( labelTable );
		goldPlatePrice.setPaintLabels(true);
		goldPlatePrice.setValue(50);
		add(goldPlate);
		add(goldPlatePrice);
		
		String[] ingredients = new String[] {"Avocado", "Crab", "Eel", "Rice", "Seaweed", "Shrimp", "Tuna", "Yellowtail"};
		// JComboBox<String> ingredient = new JComboBox<String>(ingredients);
		// ingredient.addActionListener(this);
		// JLabel selectIngredient = new JLabel("Select Ingredient (Only for Roll)");
		// add(selectIngredient);
		// add(ingredient);

		for (int i = 0; i < 8; i++) {
			JSlider ingredientAmounts = new JSlider(0, 15);
			ingredientAmounts.setPaintTrack(true); 
			ingredientAmounts.setPaintTicks(true); 
			ingredientAmounts.setPaintLabels(true); 
			ingredientAmounts.setMajorTickSpacing(1);
			Hashtable<Integer,JLabel> labelTable2 = new Hashtable<Integer,JLabel>();
			labelTable2.put(new Integer(0), new JLabel("0.0 oz"));
			labelTable2.put(new Integer(5), new JLabel("0.5 oz"));
			labelTable2.put(new Integer(10), new JLabel("1.0 oz"));
			labelTable2.put(new Integer(15), new JLabel("1.5 oz"));
			JLabel ingredientAmount = new JLabel(ingredients[i] + " Amount (Only for Roll)");
			ingredientAmounts.setLabelTable( labelTable2 );
			ingredientAmounts.setPaintLabels(true);
			ingredientAmounts.setValue(0);
			add(ingredientAmount);
			add(ingredientAmounts);
		}
		
		// JButton addIngredient = new JButton("Add Ingredient to Roll");
		// addIngredient.addActionListener(this);
		// add(addIngredient);
		
		JButton placePlate = new JButton("Place Plate");
		placePlate.addActionListener(this);
		placePlate.setActionCommand("Place");
		add(placePlate);
		
		/*
		JButton sashimi_button = new JButton("Make red plate of crab sashimi at position 3");
		sashimi_button.setActionCommand("red_crab_sashimi_at_3");
		sashimi_button.addActionListener(this);
		add(sashimi_button);

		JButton nigiri_button = new JButton("Make blue plate of eel nigiri at position 8");
		nigiri_button.setActionCommand("blue_eel_nigiri_at_8");
		nigiri_button.addActionListener(this);
		add(nigiri_button);

		JButton roll_button = new JButton("Make gold plate of KMP roll at position 5");
		roll_button.setActionCommand("gold_kmp_roll_at_5");
		roll_button.addActionListener(this);
		add(roll_button);
		*/
		
		// JButton plateInfoButton = new JButton("See plate info");
		// plateInfoButton.setActionCommand("gold_kmp_roll_at_5");
		// plateInfoButton.addActionListener(this);
		// add(plateInfoButton);
		
		kmp_roll = new Roll("KMP Roll", new IngredientPortion[] {new EelPortion(1.0), 
				new AvocadoPortion(0.5), new SeaweedPortion(0.2)});
		crab_sashimi = new Sashimi(Sashimi.SashimiType.CRAB);
		eel_nigiri = new Nigiri(Nigiri.NigiriType.EEL);
	}

	public void registerChefListener(ChefViewListener cl) {
		listeners.add(cl);
	}

	private void makeRedPlateRequest(Sushi plate_sushi, int plate_position) {
		for (ChefViewListener l : listeners) {
			l.handleRedPlateRequest(plate_sushi, plate_position);
		}
	}

	private void makeGreenPlateRequest(Sushi plate_sushi, int plate_position) {
		for (ChefViewListener l : listeners) {
			l.handleGreenPlateRequest(plate_sushi, plate_position);
		}
	}

	private void makeBluePlateRequest(Sushi plate_sushi, int plate_position) {
		for (ChefViewListener l : listeners) {
			l.handleBluePlateRequest(plate_sushi, plate_position);
		}
	}
	
	private void makeGoldPlateRequest(Sushi plate_sushi, int plate_position, double price) {
		for (ChefViewListener l : listeners) {
			l.handleGoldPlateRequest(plate_sushi, plate_position, price);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
		
		/*
		case "red_crab_sashimi_at_3":
			makeRedPlateRequest(crab_sashimi, 3);
			break;
		case "blue_eel_nigiri_at_8":
			makeBluePlateRequest(eel_nigiri, 8);
			break;
		case "gold_kmp_roll_at_5":
			makeGoldPlateRequest(kmp_roll, 5, 5.00);
		*/
		}
	}
}
